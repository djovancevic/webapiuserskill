﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using USERSKILL8.Interfaces;
using Moq;
using USERSKILL8.Models;
using USERSKILL8.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using System.Linq;

namespace USERSKILL8.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetReturnsProductWithSameId() //kategorija ut: nevazeci parametri vracaju odgovarajuci odgovor greske
        {
            //Arange
            var mockRepository = new Mock<IUserRepo>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new User { UserId = 42 });
            var controller = new UsersController(mockRepository.Object);
            //Act
            IHttpActionResult actionResult = controller.Get(42);
            var contentResult = actionResult as OkNegotiatedContentResult<User>;
            //Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.UserId);
        }

        //-------------------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void GetReturnsNotFound()  // kategorija ut: akcija vraca odgovarajuci tip odgovora (bila greska ili ok)
        {
            //Arange
            var mockRepository = new Mock<IUserRepo>();
            var controller = new UsersController(mockRepository.Object);
            //Act
            IHttpActionResult actionResult = controller.Get(10);
            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        //-------------------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void DeleteReturnsNotFound() // kategorija ut: akcija vraca odgovarajuci tip odgovora (bila greska ili ok)
        {
            //Arange
            var mockRepository = new Mock<IUserRepo>();
            var controller = new UsersController(mockRepository.Object);
            //Act
            IHttpActionResult actionResult = controller.Delete(10);
            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        //-------------------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void DeleteReturnsOk() // kategorija ut: akcija vraca odgovarajuci tip odgovora (bila greska ili ok)
        {
            //Arrange
            var mockRepository = new Mock<IUserRepo>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new User { UserId = 10 }); //kada ima mockRepo.setup onda proveravamo 2xx
            var controller = new UsersController(mockRepository.Object);
            //Act
            IHttpActionResult actionResult = controller.Delete(10);
            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        //-------------------------------------------------------------------------------------------------------------------

        //[TestMethod]
        //public void PutReturnsBadRequest() // kategorija ut: akcija vraca odgovarajuci tip odgovora (bila greska ili ok)
        //{
        //    //Arange
        //    var mockRepository = new Mock<IUserRepo>();
        //    var controller = new UsersController(mockRepository.Object); // kada nema mockRepo.setupa onda proveravamo 4xx
        //    //Act
        //    IHttpActionResult actionResult = controller.Put(10, new User { UserId = 9, Name = "User10" });
        //    //Assert
        //    Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        //}

        //-------------------------------------------------------------------------------------------------------------------

        //[TestMethod]
        //public void PostMethodSetsLocationHeader() //kategorija ut: akcija vraca odgovarajuci tip odgovora (bila greska ili ok)
        //{
        //    //Arrange
        //    var mockRepository = new Mock<IUserRepo>();
        //    var controller = new UsersController(mockRepository.Object);
        //    //Act
        //    IHttpActionResult actionResult = controller.Post(new User { UserId = 10, Name = "User11" });
        //    var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<User>;
        //    //Assert
        //    Assert.IsNotNull(createdResult);
        //    Assert.AreEqual("DefaultApi", createdResult.RouteName);
        //    Assert.AreEqual(10, createdResult.RouteValues["id"]);

        //}

        //-------------------------------------------------------------------------------------------------------------------

        //[TestMethod]
        //public void GetReturnsMultipleObjects() // verifikovanje sadrzaja liste modela u odgovoru
        //{
        //    //Arrange
        //    List<User> users = new List<User>();
        //    users.Add(new User { UserId = 1, Name = "User1" });
        //    users.Add(new User { UserId = 2, Name = "User2" });
        //    var mockRepository = new Mock<IUserRepo>();
        //    mockRepository.Setup(x => x.GetAll()).Returns(users.AsQueryable());
        //    var controller = new UsersController(mockRepository.Object);

        //    //Act
        //    IEnumerable<User> result = controller.Default();

        //    //Assert
        //    Assert.IsNotNull(result);
        //    Assert.AreEqual(users.Count, result.ToList().Count);
        //    Assert.AreEqual(users.ElementAt(0), result.ElementAt(0));
        //    Assert.AreEqual(users.ElementAt(1), result.ElementAt(1));
        //}


        //-------------------------------------------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------------------------------------
        //Isti testovi za Skills (7 testova)

        [TestMethod]
        public void GetReturnsProductWithSameId_Skill() //kategorija ut: nevazeci parametri vracaju odgovarajuci odgovor greske
        {
            //Arange
            var mockRepository = new Mock<ISkillRepo>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Skill { SkillId = 42 });
            var controller = new SkillsController(mockRepository.Object);
            //Act
            IHttpActionResult actionResult = controller.Get(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Skill>;
            //Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.SkillId);
        }

        //-------------------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void GetReturnsNotFound_Skill()  // kategorija ut: akcija vraca odgovarajuci tip odgovora (bila greska ili ok)
        {
            //Arange
            var mockRepository = new Mock<ISkillRepo>();
            var controller = new SkillsController(mockRepository.Object);
            //Act
            IHttpActionResult actionResult = controller.Get(10);
            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        //-------------------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void DeleteReturnsNotFound_Skill() // kategorija ut: akcija vraca odgovarajuci tip odgovora (bila greska ili ok)
        {
            //Arange
            var mockRepository = new Mock<ISkillRepo>();
            var controller = new SkillsController(mockRepository.Object);
            //Act
            IHttpActionResult actionResult = controller.Delete(10);
            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        //-------------------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void DeleteReturnsOk_Skill() // kategorija ut: akcija vraca odgovarajuci tip odgovora (bila greska ili ok)
        {
            //Arrange
            var mockRepository = new Mock<ISkillRepo>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new Skill { SkillId = 10 }); //kada ima mockRepo.setup onda proveravamo 2xx
            var controller = new SkillsController(mockRepository.Object);
            //Act
            IHttpActionResult actionResult = controller.Delete(10);
            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        //-------------------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void PutReturnsBadRequest_Skill() // kategorija ut: akcija vraca odgovarajuci tip odgovora (bila greska ili ok)
        {
            //Arange
            var mockRepository = new Mock<ISkillRepo>();
            var controller = new SkillsController(mockRepository.Object); // kada nema mockRepo.setupa onda proveravamo 4xx
            //Act
            IHttpActionResult actionResult = controller.Put(10, new Skill { SkillId = 9, SkillName = "User10" });
            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        //-------------------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void PostMethodSetsLocationHeader_Skill() //kategorija ut: akcija vraca odgovarajuci tip odgovora (bila greska ili ok)
        {
            //Arrange
            var mockRepository = new Mock<ISkillRepo>();
            var controller = new SkillsController(mockRepository.Object);
            //Act
            IHttpActionResult actionResult = controller.Post(new Skill { SkillId = 10, SkillName = "User11" });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Skill>;
            //Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(10, createdResult.RouteValues["id"]);

        }

        //-------------------------------------------------------------------------------------------------------------------

        //[TestMethod]
        //public void GetReturnsMultipleObjects_Skill()
        //{
        //    //Arrange
        //    List<Skill> skills = new List<Skill>();
        //    skills.Add(new Skill { SkillId = 1, SkillName = "User1" });
        //    skills.Add(new Skill { SkillId = 2, SkillName = "User2" });
        //    var mockRepository = new Mock<ISkillRepo>();
        //    mockRepository.Setup(x => x.GetAll()).Returns(skills.AsQueryable());
        //    var controller = new SkillsController(mockRepository.Object);

        //    //Act
        //    IEnumerable<Skill> result = controller.Default();

        //    //Assert
        //    Assert.IsNotNull(result);
        //    Assert.AreEqual(skills.Count, result.ToList().Count);
        //    Assert.AreEqual(skills.ElementAt(0), result.ElementAt(0));
        //    Assert.AreEqual(skills.ElementAt(1), result.ElementAt(1));
        //}

    }
}
