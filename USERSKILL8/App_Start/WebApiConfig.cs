﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Unity;
using Unity.Lifetime;
using USERSKILL8.Interfaces;
using USERSKILL8.Models;
using USERSKILL8.Repository;
using USERSKILL8.Resolver;

namespace USERSKILL8
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Unity ! moraju se obe klase registrovati
            var container = new UnityContainer();
            container.RegisterType<IUserRepo, UsersRepo>(new HierarchicalLifetimeManager());
            container.RegisterType<ISkillRepo, SkillsRepo>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);

            //self Reference ignore related to JSON serialization
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            //To enable CORS globaly
            var corsAttr = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(corsAttr);

            // to enable multipart/form-data
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue("multipart/form-data"));

            //Configuring OData endpoints
            //ODataModelBuilder builder = new ODataConventionModelBuilder();
            //builder.EntitySet<User>("Users");
            //builder.EntitySet<Skill>("Skills");
            //config.Routes.MapODataServiceRoute(
            //    routeName: "ODataRoute",
            //    routePrefix: null,
            //    model: builder.GetEdmModel());

            //to provide OData globaly for app - options for Get with Query options: sort, filter, inlineCount, paging ...
            //config.EnableQuerySupport();



        }
    }
}
