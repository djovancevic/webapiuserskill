(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _users_users_table_users_table_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./users/users-table/users-table.component */ "./src/app/users/users-table/users-table.component.ts");
/* harmony import */ var _users_add_user_add_user_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./users/add-user/add-user.component */ "./src/app/users/add-user/add-user.component.ts");
/* harmony import */ var _users_details_details_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./users/details/details.component */ "./src/app/users/details/details.component.ts");
/* harmony import */ var _users_skills_table_skills_table_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./users/skills-table/skills-table.component */ "./src/app/users/skills-table/skills-table.component.ts");
/* harmony import */ var _users_add_skill_add_skill_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./users/add-skill/add-skill.component */ "./src/app/users/add-skill/add-skill.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _registration_reg_form_reg_form_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./registration/reg-form/reg-form.component */ "./src/app/registration/reg-form/reg-form.component.ts");
/* harmony import */ var _registration_login_login_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./registration/login/login.component */ "./src/app/registration/login/login.component.ts");











var routes = [
    { path: "users", component: _users_users_table_users_table_component__WEBPACK_IMPORTED_MODULE_3__["UsersTableComponent"] },
    { path: "users/add", component: _users_add_user_add_user_component__WEBPACK_IMPORTED_MODULE_4__["AddUserComponent"] },
    { path: "users/details/:id", component: _users_details_details_component__WEBPACK_IMPORTED_MODULE_5__["DetailsComponent"] },
    { path: "users/:id", component: _users_add_user_add_user_component__WEBPACK_IMPORTED_MODULE_4__["AddUserComponent"] },
    { path: "skills", component: _users_skills_table_skills_table_component__WEBPACK_IMPORTED_MODULE_6__["SkillsTableComponent"] },
    { path: "skills/add", component: _users_add_skill_add_skill_component__WEBPACK_IMPORTED_MODULE_7__["AddSkillComponent"] },
    { path: "skills/:id", component: _users_add_skill_add_skill_component__WEBPACK_IMPORTED_MODULE_7__["AddSkillComponent"] },
    { path: "home", component: _home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"] },
    { path: "register", component: _registration_reg_form_reg_form_component__WEBPACK_IMPORTED_MODULE_9__["RegFormComponent"] },
    { path: "login", component: _registration_login_login_component__WEBPACK_IMPORTED_MODULE_10__["LoginComponent"] },
    { path: "", redirectTo: "/login", pathMatch: "full" },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n\n  <div class=\"row\">\n    <div class=\"col\">\n      <us-header></us-header>\n    </div>\n  </div>\n\n  <div class=\"row\">\n    <div class=\"col-12\">\n      <us-navbar></us-navbar>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-12\">\n      <router-outlet></router-outlet>\n    </div>\n  </div>\n  \n\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'USERSKILL2';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _core_header_header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./core/header/header.component */ "./src/app/core/header/header.component.ts");
/* harmony import */ var _core_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./core/navbar/navbar.component */ "./src/app/core/navbar/navbar.component.ts");
/* harmony import */ var _users_users_table_users_table_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./users/users-table/users-table.component */ "./src/app/users/users-table/users-table.component.ts");
/* harmony import */ var _users_skills_table_skills_table_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./users/skills-table/skills-table.component */ "./src/app/users/skills-table/skills-table.component.ts");
/* harmony import */ var _users_add_user_add_user_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./users/add-user/add-user.component */ "./src/app/users/add-user/add-user.component.ts");
/* harmony import */ var _users_add_skill_add_skill_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./users/add-skill/add-skill.component */ "./src/app/users/add-skill/add-skill.component.ts");
/* harmony import */ var _users_details_details_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./users/details/details.component */ "./src/app/users/details/details.component.ts");
/* harmony import */ var _registration_reg_form_reg_form_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./registration/reg-form/reg-form.component */ "./src/app/registration/reg-form/reg-form.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _users_filter_filter_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./users/filter/filter.component */ "./src/app/users/filter/filter.component.ts");
/* harmony import */ var _users_paging_paging_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./users/paging/paging.component */ "./src/app/users/paging/paging.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _interceptor_httpconfig_interceptor__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./interceptor/httpconfig.interceptor */ "./src/app/interceptor/httpconfig.interceptor.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _registration_login_login_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./registration/login/login.component */ "./src/app/registration/login/login.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _core_header_header_component__WEBPACK_IMPORTED_MODULE_5__["HeaderComponent"],
                _core_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_6__["NavbarComponent"],
                _users_users_table_users_table_component__WEBPACK_IMPORTED_MODULE_7__["UsersTableComponent"],
                _users_skills_table_skills_table_component__WEBPACK_IMPORTED_MODULE_8__["SkillsTableComponent"],
                _users_add_user_add_user_component__WEBPACK_IMPORTED_MODULE_9__["AddUserComponent"],
                _users_add_skill_add_skill_component__WEBPACK_IMPORTED_MODULE_10__["AddSkillComponent"],
                _users_details_details_component__WEBPACK_IMPORTED_MODULE_11__["DetailsComponent"],
                _registration_reg_form_reg_form_component__WEBPACK_IMPORTED_MODULE_12__["RegFormComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_13__["HomeComponent"],
                _users_filter_filter_component__WEBPACK_IMPORTED_MODULE_14__["FilterComponent"],
                _users_paging_paging_component__WEBPACK_IMPORTED_MODULE_15__["PagingComponent"],
                _registration_login_login_component__WEBPACK_IMPORTED_MODULE_20__["LoginComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_16__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_18__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_18__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_19__["NgbModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_21__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_22__["MatDialogModule"]
            ],
            providers: [
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_16__["HTTP_INTERCEPTORS"], useClass: _interceptor_httpconfig_interceptor__WEBPACK_IMPORTED_MODULE_17__["HttpConfigInterceptor"], multi: true }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/core/header/header.component.css":
/*!**************************************************!*\
  !*** ./src/app/core/header/header.component.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/core/header/header.component.html":
/*!***************************************************!*\
  !*** ./src/app/core/header/header.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<img src=\"../../assets/image/cuadros-azules1.jpg\" class=\"img-fluid rounded\" width=\"100%\">"

/***/ }),

/***/ "./src/app/core/header/header.component.ts":
/*!*************************************************!*\
  !*** ./src/app/core/header/header.component.ts ***!
  \*************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'us-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/core/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/core/header/header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/core/navbar/navbar.component.css":
/*!**************************************************!*\
  !*** ./src/app/core/navbar/navbar.component.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvbmF2YmFyL25hdmJhci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/core/navbar/navbar.component.html":
/*!***************************************************!*\
  !*** ./src/app/core/navbar/navbar.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-sm navbar-light bg-light\">\n\n\t<a class=\"navbar-brand\" routerLink=\"/home\" routerLinkActive=\"active\" >Users & Skills</a>\n\n\t<ul class=\"navbar-nav mr-auto\">\n\t\t<li class=\"nav-item\">\n\t\t\t<a class=\"nav-link\" routerLink=\"/users\" routerLinkActive=\"active\">Users</a>\n    </li>\n    \n    <li class=\"nav-item\">\n\t\t\t<a class=\"nav-link\" routerLink=\"/skills\" routerLinkActive=\"active\">Skills</a>\n\t\t</li>\n\n\t\t<li class=\"nav-item\">\n\t\t\t<a class=\"nav-link\" routerLink=\"users/add\" routerLinkActive=\"active\">Add user</a>\n    </li>\t\n    \n    <li class=\"nav-item\">\n\t\t\t<a class=\"nav-link\" routerLink=\"skills/add\" routerLinkActive=\"active\">Add skill</a>\n\t\t</li>\t\n\t</ul>\n\n\t<ul class=\"navbar-nav ml-auto\">   \n\t\t<li class=\"nav-item\">\n\t\t\t<a class=\"nav-link\" routerLink=\"login\" routerLinkActive=\"active\">Login/Register</a>\n\t\t</li>\n\t</ul>\n</nav>"

/***/ }),

/***/ "./src/app/core/navbar/navbar.component.ts":
/*!*************************************************!*\
  !*** ./src/app/core/navbar/navbar.component.ts ***!
  \*************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent() {
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'us-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/core/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/core/navbar/navbar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 *ngIf=\"flag\">Hi {{cU}}!</h1>\n<p *ngIf=\"flag\">You're logged in Users&Skills</p>\n<p *ngIf=\"!flag\">You're logged out from Users&Skills</p>\n<!--\n<h3>All registered users:</h3>\n<ul>\n    <li *ngFor=\"let user of users\">\n        {{user.username}} ({{user.firstName}} {{user.lastName}})\n        - <a (click)=\"deleteUser(user.id)\" class=\"text-danger\">Delete</a>\n    </li>\n</ul>\n-->\n\n<div class=\"form-group\" >\n  <button *ngIf=\"flag\" class=\"btn btn-primary\" (click)=\"onLogout($event)\">Logout</button>\n</div>"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _users_service_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../users/service/authentication.service */ "./src/app/users/service/authentication.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var HomeComponent = /** @class */ (function () {
    function HomeComponent(authenticationService, router) {
        this.authenticationService = authenticationService;
        this.router = router;
        this.flag = false;
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        //console.log("currentUser from localStorage je:  ", this.currentUser);
        if (this.currentUser) {
            this.flag = true;
            this.cU = this.currentUser.userName;
            console.log("cU je:  ", this.cU);
        }
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent.prototype.onLogout = function (event) {
        this.router.navigate(['login']);
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'us-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_users_service_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/interceptor/httpconfig.interceptor.ts":
/*!*******************************************************!*\
  !*** ./src/app/interceptor/httpconfig.interceptor.ts ***!
  \*******************************************************/
/*! exports provided: HttpConfigInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpConfigInterceptor", function() { return HttpConfigInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");




var HttpConfigInterceptor = /** @class */ (function () {
    function HttpConfigInterceptor() {
    }
    //function which will be called for all http calls
    HttpConfigInterceptor.prototype.intercept = function (request, next) {
        //treba iskljuciti interceptor za login, register i get http methods
        //let isRegisterRequest : boolean = request.url.endsWith('/register');
        var isPhoto = request.url.endsWith('.jpg');
        //if( !isRegisterRequest && !isLoginRequest ){
        //how to update the request Parameters
        var token;
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser) {
            token = currentUser.access_token;
        }
        else {
            token = "";
        }
        console.log("Token dobavljen sa localStorage.getItem:   ", token);
        if (token) {
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        }
        if (!request.headers.has('Content-Type')) {
            if (!isPhoto) {
                request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
            }
            else {
                request = request.clone({ headers: request.headers.set('Content-Type', 'image/jpeg') });
            }
        }
        if (!isPhoto) {
            request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
        }
        //logging the updated Parameters to browser's console
        console.log("Before making api call : ", token);
        return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (event) {
            //logging the http response to browser's console in case of a success
            if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpResponse"]) {
                console.log("api call success :", event);
            }
        }, function (error) {
            //logging the http response to browser's console in case of a failuer
            if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpResponse"]) {
                console.log("api call error :", event);
            }
        }));
        //}
    };
    HttpConfigInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HttpConfigInterceptor);
    return HttpConfigInterceptor;
}());



/***/ }),

/***/ "./src/app/registration/login/login.component.css":
/*!********************************************************!*\
  !*** ./src/app/registration/login/login.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdHJhdGlvbi9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/registration/login/login.component.html":
/*!*********************************************************!*\
  !*** ./src/app/registration/login/login.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-4\"></div>\n    <div class=\"col-4\">\n      <h2>Login</h2>\n      <form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\">\n        <div class=\"form-group\">\n          <label for=\"username\">Email</label>\n          <input type=\"text\" formControlName=\"username\" class=\"form-control\"\n            [ngClass]=\"{ 'is-invalid': submitted && f.username.errors }\" />\n          <div *ngIf=\"submitted && f.username.errors\" class=\"invalid-feedback\">\n            <div *ngIf=\"f.username.errors.required\">Email is required</div>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label for=\"password\">Password</label>\n          <input type=\"password\" formControlName=\"password\" class=\"form-control\"\n            [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" />\n          <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n            <div *ngIf=\"f.password.errors.required\">Password is required</div>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <button [disabled]=\"loading\" class=\"btn btn-primary\">Login</button>\n          <a [routerLink]=\"['/register']\" class=\"btn btn-link\">Register</a>\n        </div>\n      </form>\n    </div>\n    <div class=\"col-4\"></div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/registration/login/login.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/registration/login/login.component.ts ***!
  \*******************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _users_service_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../users/service/authentication.service */ "./src/app/users/service/authentication.service.ts");






var LoginComponent = /** @class */ (function () {
    function LoginComponent(formBuilder, route, router, authenticationService) {
        this.formBuilder = formBuilder;
        this.route = route;
        this.router = router;
        this.authenticationService = authenticationService;
        this.loading = false;
        this.submitted = false;
    } //private alertService: AlertService
    LoginComponent.prototype.ngOnInit = function () {
        this.loginForm = this.formBuilder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
        // reset login status
        this.authenticationService.logout();
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    };
    Object.defineProperty(LoginComponent.prototype, "f", {
        // convenience getter for easy access to form fields
        get: function () { return this.loginForm.controls; },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.loading = true;
        this.authenticationService.login(this.f.username.value, this.f.password.value)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["first"])())
            .subscribe(function (data) {
            _this.router.navigate(["home"]);
        }, function (error) {
            //this.alertService.error(error);
            console.log('Greska prilikom autentikacije na serveru.');
            _this.loading = false;
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'us-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/registration/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/registration/login/login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _users_service_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/registration/reg-form/reg-form.component.css":
/*!**************************************************************!*\
  !*** ./src/app/registration/reg-form/reg-form.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdHJhdGlvbi9yZWctZm9ybS9yZWctZm9ybS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/registration/reg-form/reg-form.component.html":
/*!***************************************************************!*\
  !*** ./src/app/registration/reg-form/reg-form.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>{{ registerUserForm.value | json}}</p>\n<p>{{ registerUserForm.status | json}}</p>\n<div class=\"container\">\n\t<div class=\"row\">\n\t\t<div class=\"col-3\"></div>\n\t\t<div class=\"col-6\">\n\t\t\t<h1>Register</h1>\n\t\t\t<form [formGroup]=\"registerUserForm\" (submit)=\"onSubmit()\">\n\n\n\t\t\t\t<div class=\"form-group row\">\n\t\t\t\t\t<div class=\"col-sm-8\">\n\t\t\t\t\t\t<label for=\"email\">Email</label>\n\t\t\t\t\t\t<input class=\"form-control\" type=\"email\" name=\"email\" id=\"email\" placeholder=\"Email\"\n\t\t\t\t\t\t\tformControlName=\"email\">\n\n\t\t\t\t\t\t<small\n\t\t\t\t\t\t\t*ngIf=\"registerUserForm.controls.email.touched && registerUserForm.controls.email.errors?.required\"\n\t\t\t\t\t\t\tclass=\"text-danger\">Email is required</small>\n\n\t\t\t\t\t\t<small\n\t\t\t\t\t\t\t*ngIf=\"registerUserForm.controls.email.touched && !registerUserForm.controls.email.errors?.required && registerUserForm.controls.email.errors?.email\"\n\t\t\t\t\t\t\tclass=\"text-danger\">Email is invalid</small>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"form-group row\">\n\n\t\t\t\t\t<div class=\"col-sm-8\">\n\t\t\t\t\t\t<label for=\"password\">Password</label>\n\t\t\t\t\t\t<input class=\"form-control\" type=\"password\" name=\"password\" id=\"password\" placeholder=\"Password\"\n\t\t\t\t\t\t\tformControlName=\"password\">\n\n\t\t\t\t\t\t<small\n\t\t\t\t\t\t\t*ngIf=\"registerUserForm.controls.password.touched && registerUserForm.controls.password.errors?.required\"\n\t\t\t\t\t\t\tclass=\"text-danger\">Password is required</small>\n\n\t\t\t\t\t\t<small\n\t\t\t\t\t\t\t*ngIf=\"registerUserForm.controls.password.touched && registerUserForm.controls.password.errors?.minlength\"\n\t\t\t\t\t\t\tclass=\"text-danger\">Minimal password length is 8</small>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"form-group row\">\n\n\t\t\t\t\t<div class=\"col-sm-8\">\n\t\t\t\t\t\t<label for=\"password\">Confirm password</label>\n\t\t\t\t\t\t<input class=\"form-control\" type=\"password\" name=\"confirmPassword\" id=\"cPassword\"\n\t\t\t\t\t\t\tplaceholder=\"Confirm password\" formControlName=\"confirmPassword\">\n\n\t\t\t\t\t\t<small\n\t\t\t\t\t\t\t*ngIf=\"registerUserForm.controls.confirmPassword.touched && registerUserForm.controls.confirmPassword.errors?.required\"\n\t\t\t\t\t\t\tclass=\"text-danger\">Confirm assword is required</small>\n\n\t\t\t\t\t\t<small\n\t\t\t\t\t\t\t*ngIf=\"registerUserForm.controls.confirmPassword.touched && registerUserForm.controls.confirmPassword.errors?.minlength\"\n\t\t\t\t\t\t\tclass=\"text-danger\">Minimal password length is 8</small>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"form-group row float-center\">\n\t\t\t\t\t<div class=\"col\">\n\t\t\t\t\t\t<button class=\"btn btn-primary\" type=\"submit\"\n\t\t\t\t\t\t\t[disabled]=\"registerUserForm.invalid\">Submit</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</form>\n\t\t</div>\n\t\t<div class=\"col-3\"></div>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/registration/reg-form/reg-form.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/registration/reg-form/reg-form.component.ts ***!
  \*************************************************************/
/*! exports provided: RegFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegFormComponent", function() { return RegFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _users_service_users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../users/service/users.service */ "./src/app/users/service/users.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");






var RegFormComponent = /** @class */ (function () {
    function RegFormComponent(fb, userService, router) {
        this.fb = fb;
        this.userService = userService;
        this.router = router;
        this.loading = false;
        this.submitted = false;
        this.createForm();
    }
    RegFormComponent.prototype.createForm = function () {
        this.registerUserForm = this.fb.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(8)]],
            confirmPassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(8)]]
        });
    };
    RegFormComponent.prototype.ngOnInit = function () { };
    RegFormComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        // stop here if form is invalid
        if (this.registerUserForm.invalid) {
            return;
        }
        this.loading = true;
        this.userService.registerUser(this.registerUserForm.value)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])())
            .subscribe(function (data) {
            //this.alertService.success('Registration successful', true);
            _this.router.navigate(['/login']);
        }, function (error) {
            //this.alertService.error(error);
            console.log("Greska pri registraciji korisnika.");
            _this.loading = false;
        });
    };
    RegFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'us-reg-form',
            template: __webpack_require__(/*! ./reg-form.component.html */ "./src/app/registration/reg-form/reg-form.component.html"),
            styles: [__webpack_require__(/*! ./reg-form.component.css */ "./src/app/registration/reg-form/reg-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _users_service_users_service__WEBPACK_IMPORTED_MODULE_3__["UsersService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], RegFormComponent);
    return RegFormComponent;
}());



/***/ }),

/***/ "./src/app/users/add-skill/add-skill.component.css":
/*!*********************************************************!*\
  !*** ./src/app/users/add-skill/add-skill.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL2FkZC1za2lsbC9hZGQtc2tpbGwuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/users/add-skill/add-skill.component.html":
/*!**********************************************************!*\
  !*** ./src/app/users/add-skill/add-skill.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>{{skillForm.value | json}}</p>\n<p>{{skillForm.status | json}}</p>\n\n\n\n<div class=\"row\">\n\n  <div class=\"col-2\"></div>\n\n  <div class=\"col-8\">\n\n\n    <form [formGroup]=\"skillForm\" (submit)=\"onSubmit()\">\n      <div class=\"form-group row\">\n        <label for=\"name\" class=\"col-2 col-form-label\">Skill name</label>\n        <div class=\"col-10\">\n          <input class=\"form-control\" type=\"text\" id=\"skillName\" placeholder=\"Skill Name\" formControlName=\"SkillName\">\n\n          <small class=\"text-danger\" *ngIf=\"skillForm.controls.SkillName.touched && skillForm.controls.SkillName.errors?.required\">\n            Skill name is required</small>\n\n        </div>\n      </div>\n\n      \n      <div class=\"form-group row float-right\">\n        <div class=\"col-12\">\n          <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"skillForm.invalid\">Submit</button>\n        </div>\n      </div>\n    </form>\n\n  </div>\n\n  <div class=\"col-2\"></div>\n\n</div>\n\n"

/***/ }),

/***/ "./src/app/users/add-skill/add-skill.component.ts":
/*!********************************************************!*\
  !*** ./src/app/users/add-skill/add-skill.component.ts ***!
  \********************************************************/
/*! exports provided: AddSkillComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddSkillComponent", function() { return AddSkillComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_skill__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/skill */ "./src/app/users/model/skill.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_users_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/users.service */ "./src/app/users/service/users.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var AddSkillComponent = /** @class */ (function () {
    function AddSkillComponent(fb, userService, router) {
        this.fb = fb;
        this.userService = userService;
        this.router = router;
        this.createForm();
    }
    AddSkillComponent.prototype.ngOnInit = function () {
        //edit TO DO
    };
    AddSkillComponent.prototype.createForm = function () {
        this.skillForm = this.fb.group({
            'SkillName': ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    };
    AddSkillComponent.prototype.onSubmit = function () {
        var _this = this;
        var submittedSkill = new _model_skill__WEBPACK_IMPORTED_MODULE_2__["Skill"](this.skillForm.value);
        if (this.skill && this.skill.SkillId) {
            // edit TO DO
        }
        else {
            this.userService.addSkill(submittedSkill).subscribe(function (skill) {
                _this.skillForm.reset();
                _this.router.navigate(['/skills']);
            });
        }
    };
    AddSkillComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'us-add-skill',
            template: __webpack_require__(/*! ./add-skill.component.html */ "./src/app/users/add-skill/add-skill.component.html"),
            styles: [__webpack_require__(/*! ./add-skill.component.css */ "./src/app/users/add-skill/add-skill.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _service_users_service__WEBPACK_IMPORTED_MODULE_4__["UsersService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], AddSkillComponent);
    return AddSkillComponent;
}());



/***/ }),

/***/ "./src/app/users/add-user/add-user.component.css":
/*!*******************************************************!*\
  !*** ./src/app/users/add-user/add-user.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL2FkZC11c2VyL2FkZC11c2VyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/users/add-user/add-user.component.html":
/*!********************************************************!*\
  !*** ./src/app/users/add-user/add-user.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>{{userForm.value | json}}</p>\n<p>{{userForm.status | json}}</p>\n\n\n\n<div class=\"row\">\n\n  <div class=\"col-2\"></div>\n\n  <div class=\"col-8\">\n\n\n    <form [formGroup]=\"userForm\" (submit)=\"onSubmit()\">\n      <div class=\"form-group row\">\n        <label for=\"name\" class=\"col-2 col-form-label\">Name</label>\n        <div class=\"col-10\">\n          <input class=\"form-control\" type=\"text\" id=\"name\" placeholder=\"Name\" formControlName=\"Name\">\n\n          <small class=\"text-danger\" *ngIf=\"userForm.controls.Name.touched && userForm.controls.Name.errors?.required\">\n            Name is required</small>\n\n        </div>\n      </div>\n\n      <div class=\"form-group row\">\n        <label for=\"email\" class=\"col-2 col-form-label\">Email</label>\n        <div class=\"col-10\">\n          <input class=\"form-control\" type=\"text\" id=\"email\" placeholder=\"Email\" formControlName=\"Email\">\n          <small class=\"text-danger\"\n            *ngIf=\"userForm.controls.Email.touched && userForm.controls.Email.errors?.required\">\n            Email is required</small>\n        </div>\n      </div>\n\n      <div class=\"form-group row\">\n        <label for=\"password\" class=\"col-2 col-form-label\">Password</label>\n        <div class=\"col-10\">\n          <input class=\"form-control\" type=\"text\" id=\"password\" placeholder=\"Password\" formControlName=\"Password\">\n          <small class=\"text-danger\"\n            *ngIf=\"userForm.controls.Password.touched && userForm.controls.Password.errors?.required\">\n            Password is required</small>\n        </div>\n      </div>\n\n      <div class=\"form-group row\">\n        <label for=\"photo\" class=\"col-2 col-form-label\">Photo</label>\n        <div class=\"col-10\">\n          <input class=\"form-control\" type=\"text\" id=\"photo\" placeholder=\"PhotoLink\" formControlName=\"Photo\">\n          <small class=\"text-danger\"\n            *ngIf=\"userForm.controls.Photo.touched && userForm.controls.Photo.errors?.required\">\n            Photo is required</small>\n        </div>\n      </div>\n\n      <div class=\"form-group row\">\n        <label class=\"col-2 col-form-label\">Skills of user:</label>\n        <div class=\"col-10\">\n          <select  name=\"selectedSkills[]\" formControlName=\"Skills\" multiple [compareWith]=\"compareFn\" >\n\n            <option *ngFor=\"let skill of skills;\" [ngValue]=\"skill\" >{{skill.SkillName}}</option>\n\n          </select>\n        </div>\n      </div>\n\n\n\n      <div class=\"form-group row float-right\">\n        <div class=\"col-12\">\n          <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"userForm.invalid\">Submit</button>\n        </div>\n      </div>\n    </form>\n\n  </div>\n\n  <div class=\"col-2\"></div>\n\n</div>"

/***/ }),

/***/ "./src/app/users/add-user/add-user.component.ts":
/*!******************************************************!*\
  !*** ./src/app/users/add-user/add-user.component.ts ***!
  \******************************************************/
/*! exports provided: AddUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUserComponent", function() { return AddUserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_user__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/user */ "./src/app/users/model/user.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_users_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/users.service */ "./src/app/users/service/users.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var AddUserComponent = /** @class */ (function () {
    function AddUserComponent(skillService, fb, userService, router, activeRoute) {
        this.skillService = skillService;
        this.fb = fb;
        this.userService = userService;
        this.router = router;
        this.activeRoute = activeRoute;
        this.skills = [{ "SkillId": 3, "SkillName": "Web Designer", "Users": [] }];
        this.params = {
            $orderby: 'SkillName',
            $filter: "substringof('', SkillName)",
            $top: 10,
            $skip: 0,
            $inlinecount: 'allpages'
        };
        this.createForm();
    }
    AddUserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getSkills();
        //edit user
        var id = this.activeRoute.snapshot.params.id;
        if (id) {
            this.userService.getUser(Number(id)).subscribe(function (user) {
                _this.user = user;
                _this.userForm.patchValue(_this.user);
            });
        }
        //preporuka je da se Skills objekat prebaci u niz id-ova radi lakseg mapiranja
        // let skills = [1,3];
        // this.user = new User({UserId:1,Name:"Petar", Skills:skills});
        // this.userForm.patchValue(this.user);
    };
    AddUserComponent.prototype.createForm = function () {
        this.userForm = this.fb.group({
            'Name': ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            'Email': ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            'Password': ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            'Photo': ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            'Skills': [[{}]] // ovo treba da je select-box element koji prima niz skill objekata
        });
    };
    AddUserComponent.prototype.getSkills = function () {
        var _this = this;
        this.skillService.getAllSkills(this.params).subscribe(function (data) {
            _this.skills = data.Items;
            for (var i = 0; i < _this.skills.length; i++) {
                _this.skills[i].Users = [];
            }
            return _this.skills;
        });
    };
    AddUserComponent.prototype.onSubmit = function () {
        var _this = this;
        var submittedUser = new _model_user__WEBPACK_IMPORTED_MODULE_2__["User"](this.userForm.value);
        if (this.user && this.user.UserId) {
            submittedUser.UserId = this.user.UserId;
            this.userService.updateUser(submittedUser).subscribe(function (user) {
                _this.userForm.reset();
                _this.router.navigate(['/users']);
                console.log("Editovani user ima sledece skillove: ", user.Skills);
            });
        }
        else {
            this.userService.addUser(submittedUser).subscribe(function (user) {
                _this.userForm.reset();
                _this.router.navigate(['/users']);
                console.log("Na serveru je kreiran novi user sa skilovima: ", user.Skills);
            });
        }
    };
    // applyClasses(skill :Skill) :boolean {
    //   let formControlValue = this.userForm.get('Skills').value;
    //   console.log('Trenutna vrednost kontrole Skills:', formControlValue )
    //   //for(let i=1; i<Number(this.userForm.get('Skills'))
    //   return true;
    // }
    AddUserComponent.prototype.compareFn = function (c1, c2) {
        var flag;
        return flag = c1 && c2 ? c1.SkillId === c2.SkillId : c1 === c2; // ako je c1&&c2 = true (a uvek jeste jer su truthy) onda je flag = boolean vrednosti levo od :; izraz c1===c2 je uvek false.
    };
    AddUserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'us-add-user',
            template: __webpack_require__(/*! ./add-user.component.html */ "./src/app/users/add-user/add-user.component.html"),
            styles: [__webpack_require__(/*! ./add-user.component.css */ "./src/app/users/add-user/add-user.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_users_service__WEBPACK_IMPORTED_MODULE_4__["UsersService"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _service_users_service__WEBPACK_IMPORTED_MODULE_4__["UsersService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], AddUserComponent);
    return AddUserComponent;
}());



/***/ }),

/***/ "./src/app/users/details/details.component.css":
/*!*****************************************************!*\
  !*** ./src/app/users/details/details.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL2RldGFpbHMvZGV0YWlscy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/users/details/details.component.html":
/*!******************************************************!*\
  !*** ./src/app/users/details/details.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"container\">\n    <div class=\"row\" style=\"background-color:rgb(243, 237, 237)\">\n      <div class=col-4></div>\n      <div class=col-4>\n        <h1>Users details </h1>\n      </div>\n      <div class=col-4></div>\n    </div>\n  </div>\n  <div class=\"container\">\n    <div class=\"row\">\n\n        <div class=\"col-2\" style=\"background-color: rgb(248, 244, 244)\">\n          <img [src]=\"photoToShow\" class=\"img-fluid rounded-circle \" alt=\"Place image title\"\n            *ngIf=\"!isPhotoLoading; else noPhotoFound\">\n          <ng-template #noPhotoFound>\n            <img src=\"../../../assets/image/NoPhotoIcon.png\" class=\"img-fluid rounded-circle \" alt=\"user.Name\">\n          </ng-template>\n\n\n          <p class=\"ui-description\" style=\"display:none\"><strong>Edit user:</strong> <a href=\"...\">Edit-page</a>\n\n        </div>\n      \n\n        <div class=\"col-10\" style=\"background-color: rgb(238, 227, 227)\">\n          <div class=\"row\">\n            <label>&nbsp; &nbsp;Name:</label>\n            <p><b>&nbsp; &nbsp; {{user.Name}}</b></p>\n          </div>\n          <div class=\"row\">\n            <label>&nbsp; &nbsp;Email: </label> <p>&nbsp; &nbsp; {{user.Email}}</p>\n          </div>\n          <div class=\"row\">\n            <label>&nbsp; &nbsp;Password: </label>\n            <p>&nbsp; &nbsp; {{user.Password}}</p>\n          </div>\n          <div class=\"row\">\n            <table>\n              <thead>\n                <tr>\n                  <th><b>&nbsp; &nbsp; Skills</b></th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let skill of user?.Skills\">\n\n                  <td>&nbsp; &nbsp; - {{skill.SkillName}} </td>\n\n                </tr>\n\n              </tbody>\n            </table>\n\n          </div>\n\n        </div>\n      \n      \n    </div>\n  </div>\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n<!--\n<img src=\"http://www.indirapuramschool.com/school-admin/uploads/staff/1547125466.jpg\" alt=\"\" width=\"220\" height=\"140\"\n  class=\"entry-image\">\n\n<figure class=\"ph\" data-loadfile=\"img/content_ph02.jpg\"\n  style=\"background-image: url(&quot;https://japanese-languageschool.com/teacher/img/content_ph02.jpg&quot;);\"></figure>\n\n\n\n<table>\n\n  <tbody>\n    <tr></tr>\n    <tr>\n      <td><b>Name: </b></td>\n      <td>Mr. Ajay Dobhal </td>\n    </tr>\n    <tr>\n      <td><b>Qulification: </b></td>\n      <td>M.C.A, P.HD, NIIT, O Level </td>\n    </tr>\n    <tr>\n      <td><b>Classes Taken: </b></td>\n      <td>X,VIII,IX,XI,XII </td>\n    </tr>\n    <tr>\n      <td><b>Subject: </b></td>\n      <td>Computer </td>\n    </tr>\n    <tr>\n      <td><b>Designation: </b></td>\n      <td>Sr. Coordinator </td>\n    </tr>\n\n  </tbody>\n</table>\n-->"

/***/ }),

/***/ "./src/app/users/details/details.component.ts":
/*!****************************************************!*\
  !*** ./src/app/users/details/details.component.ts ***!
  \****************************************************/
/*! exports provided: DetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsComponent", function() { return DetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/users.service */ "./src/app/users/service/users.service.ts");
/* harmony import */ var _model_user__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/user */ "./src/app/users/model/user.ts");





var DetailsComponent = /** @class */ (function () {
    function DetailsComponent(userService, activeRoute) {
        this.userService = userService;
        this.activeRoute = activeRoute;
        this.isPhotoLoading = true;
        this.user = new _model_user__WEBPACK_IMPORTED_MODULE_4__["User"]();
    }
    DetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        // user details
        var id = this.activeRoute.snapshot.params.id;
        if (id) {
            this.userService.getUser(Number(id)).subscribe(function (user) {
                _this.user = user;
                console.log("User name: ", _this.user.Name, " photoUrl: ", _this.user.Photo);
                _this.getPhotoService();
            });
        }
    };
    // loaduje image/jpeg kao dogadjaj pomocu FileReadera na osnovu Observable<Blob> koji dobija iz getPhotoService  
    DetailsComponent.prototype.createPhotoFromBlob = function (photo) {
        var _this = this;
        var reader = new FileReader();
        reader.addEventListener("load", function () {
            _this.photoToShow = reader.result; //reader.result je string
        }, false); // false je opcioni parametar koji sledi nakon lambda fn
        if (photo) {
            reader.readAsDataURL(photo);
        }
    };
    DetailsComponent.prototype.getPhotoService = function () {
        var _this = this;
        this.isPhotoLoading = true;
        this.userService.getPhoto(this.user).subscribe(function (data) {
            _this.createPhotoFromBlob(data);
            _this.isPhotoLoading = false;
        }, function (error) {
            _this.isPhotoLoading = true;
            console.log("Dogodila se greska prilikom poziva servisa", error.statusText);
        });
    };
    DetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'us-details',
            template: __webpack_require__(/*! ./details.component.html */ "./src/app/users/details/details.component.html"),
            styles: [__webpack_require__(/*! ./details.component.css */ "./src/app/users/details/details.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_users_service__WEBPACK_IMPORTED_MODULE_3__["UsersService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], DetailsComponent);
    return DetailsComponent;
}());



/***/ }),

/***/ "./src/app/users/filter/filter.component.css":
/*!***************************************************!*\
  !*** ./src/app/users/filter/filter.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL2ZpbHRlci9maWx0ZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/users/filter/filter.component.html":
/*!****************************************************!*\
  !*** ./src/app/users/filter/filter.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"form-inline\" [formGroup]=\"searchFormByEmail\" (submit)=\"searchByEmail()\" *ngIf=\"flagEmail\">\n\n  <input type=\"text\" class=\"form-control col-sm-4\" id=\"inlineFormInput\" placeholder=\"Search by Email\"\n    formControlName=\"controlByEmail\">\n\n  <button type=\"submit\" class=\"btn btn-primary\"><span class=\"fa fa-search\"></span></button>\n</form>\n\n\n<form class=\"form-inline\" [formGroup]=\"searchFormBySkill\" (submit)=\"searchBySkill()\" *ngIf=\"flagSkill\">\n\n  <input type=\"text\" class=\"form-control col-sm-4\" id=\"inlineFormInput\" placeholder=\"Search by Skill\"\n    formControlName=\"controlBySkill\">\n\n  <button type=\"submit\" class=\"btn btn-primary\"><span class=\"fa fa-search\"></span></button>\n</form>"

/***/ }),

/***/ "./src/app/users/filter/filter.component.ts":
/*!**************************************************!*\
  !*** ./src/app/users/filter/filter.component.ts ***!
  \**************************************************/
/*! exports provided: FilterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterComponent", function() { return FilterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");





var FilterComponent = /** @class */ (function () {
    function FilterComponent(fb) {
        this.fb = fb;
        this.onSearchByEmail = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onSearchBySkill = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        // this.flagEmail = 0;
        // this.flagSkill = 0;
        this.searchFormByEmail = this.fb.group({
            'controlByEmail': ''
        });
        this.searchFormBySkill = this.fb.group({
            'controlBySkill': ''
        });
    }
    FilterComponent.prototype.ngOnInit = function () { };
    FilterComponent.prototype.ngOnChange = function () {
        if (this.flagEmail) {
            this.flagSkill = 0;
        }
        else {
            this.flagEmail = 0;
        }
    };
    FilterComponent.prototype.searchByEmail = function () {
        this.onSearchByEmail.emit(this.searchFormByEmail.value.controlByEmail); //onSearch je eventEmitter, searchForm je naziv grupe, a serachByEmail je naziv controle u grupi
    };
    FilterComponent.prototype.searchBySkill = function () {
        this.onSearchBySkill.emit(this.searchFormBySkill.value.controlBySkill);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], FilterComponent.prototype, "onSearchByEmail", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], FilterComponent.prototype, "onSearchBySkill", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], FilterComponent.prototype, "flagEmail", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], FilterComponent.prototype, "flagSkill", void 0);
    FilterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'us-filter',
            template: __webpack_require__(/*! ./filter.component.html */ "./src/app/users/filter/filter.component.html"),
            styles: [__webpack_require__(/*! ./filter.component.css */ "./src/app/users/filter/filter.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
    ], FilterComponent);
    return FilterComponent;
}());



/***/ }),

/***/ "./src/app/users/model/skill.ts":
/*!**************************************!*\
  !*** ./src/app/users/model/skill.ts ***!
  \**************************************/
/*! exports provided: Skill */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Skill", function() { return Skill; });
var Skill = /** @class */ (function () {
    function Skill(obj) {
        this.SkillId = obj && obj.SkillId || null;
        this.SkillName = obj && obj.SkillName || "";
        this.Users = obj && obj.Users || [];
    }
    return Skill;
}());



/***/ }),

/***/ "./src/app/users/model/skillSR.ts":
/*!****************************************!*\
  !*** ./src/app/users/model/skillSR.ts ***!
  \****************************************/
/*! exports provided: SkillSR */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SkillSR", function() { return SkillSR; });
/* harmony import */ var _skill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./skill */ "./src/app/users/model/skill.ts");

var SkillSR = /** @class */ (function () {
    function SkillSR(obj) {
        this.Count = obj && obj.Count || 0;
        this.Items = obj && obj.Items.map(function (elem) { return new _skill__WEBPACK_IMPORTED_MODULE_0__["Skill"](elem); }) || [];
        this.NextPageLink = null;
    }
    return SkillSR;
}());



/***/ }),

/***/ "./src/app/users/model/user.ts":
/*!*************************************!*\
  !*** ./src/app/users/model/user.ts ***!
  \*************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User(obj) {
        this.UserId = obj && obj.UserId || null;
        this.Name = obj && obj.Name || "";
        this.Email = obj && obj.Email || "";
        this.Password = obj && obj.Password || "";
        this.Picture = obj && obj.Picture || false;
        this.Photo = obj && obj.Photo || "";
        this.Skills = obj && obj.Skills || [];
    }
    return User;
}());



/***/ }),

/***/ "./src/app/users/model/userSR.ts":
/*!***************************************!*\
  !*** ./src/app/users/model/userSR.ts ***!
  \***************************************/
/*! exports provided: UserSR */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserSR", function() { return UserSR; });
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user */ "./src/app/users/model/user.ts");

var UserSR = /** @class */ (function () {
    function UserSR(obj) {
        this.Count = obj && obj.Count || 0;
        this.Items = obj && obj.Items.map(function (elem) { return new _user__WEBPACK_IMPORTED_MODULE_0__["User"](elem); }) || [];
        this.NextPageLink = null;
    }
    return UserSR;
}());



/***/ }),

/***/ "./src/app/users/paging/paging.component.css":
/*!***************************************************!*\
  !*** ./src/app/users/paging/paging.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL3BhZ2luZy9wYWdpbmcuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/users/paging/paging.component.html":
/*!****************************************************!*\
  !*** ./src/app/users/paging/paging.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ngb-pagination [collectionSize]=\"collectionSize\" [(page)]=\"page\" [boundaryLinks]=\"true\" (pageChange)=\"pageChange()\"></ngb-pagination>\n\n<hr>\n\n<pre>Current page: {{page}}</pre>\n"

/***/ }),

/***/ "./src/app/users/paging/paging.component.ts":
/*!**************************************************!*\
  !*** ./src/app/users/paging/paging.component.ts ***!
  \**************************************************/
/*! exports provided: PagingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagingComponent", function() { return PagingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");



var PagingComponent = /** @class */ (function () {
    function PagingComponent() {
        this.onPageSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.page = 1;
    }
    PagingComponent.prototype.ngOnInit = function () {
    };
    PagingComponent.prototype.ngOnChanges = function () {
        console.log("Iz PagingComp ngOnChange collectionSize= ", this.collectionSize);
    };
    PagingComponent.prototype.pageChange = function () {
        this.onPageSelected.emit(this.page);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], PagingComponent.prototype, "collectionSize", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], PagingComponent.prototype, "onPageSelected", void 0);
    PagingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'us-paging',
            template: __webpack_require__(/*! ./paging.component.html */ "./src/app/users/paging/paging.component.html"),
            styles: [__webpack_require__(/*! ./paging.component.css */ "./src/app/users/paging/paging.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PagingComponent);
    return PagingComponent;
}());



/***/ }),

/***/ "./src/app/users/service/authentication.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/users/service/authentication.service.ts ***!
  \*********************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http) {
        this.http = http;
    }
    AuthenticationService.prototype.login = function (username, password) {
        var userData = "grant_type=password" + "&username=" + username + "&password=" + password;
        console.log(userData);
        var reqHeader = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this.http.post('http://localhost:64738/Token', userData, { headers: reqHeader })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (credentials) {
            // login successful if there's a jwt token in the response
            if (credentials && credentials.access_token) {
                console.log("Ovo je poruka ako ima access_token ", JSON.stringify(credentials));
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(credentials));
            }
            return credentials;
        }));
    };
    AuthenticationService.prototype.logout = function () {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        console.log("Current user je izbrisan ", localStorage.getItem('currentUser'));
        localStorage.clear();
    };
    AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], AuthenticationService);
    return AuthenticationService;
}());



/***/ }),

/***/ "./src/app/users/service/users.service.ts":
/*!************************************************!*\
  !*** ./src/app/users/service/users.service.ts ***!
  \************************************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _model_userSR__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/userSR */ "./src/app/users/model/userSR.ts");
/* harmony import */ var _model_skillSR__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../model/skillSR */ "./src/app/users/model/skillSR.ts");
/* harmony import */ var _model_user__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../model/user */ "./src/app/users/model/user.ts");
/* harmony import */ var _model_skill__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../model/skill */ "./src/app/users/model/skill.ts");








var usersUrl = "http://localhost:64738/api/users";
var skillsUrl = "http://localhost:64738/api/skills";
var UsersService = /** @class */ (function () {
    function UsersService(http) {
        this.http = http;
    }
    UsersService.prototype.getAll = function (params) {
        var queryString = {};
        if (params) {
            queryString = {
                params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                    .set("$orderby", params.$orderby || '')
                    .set("$filter", params.$filter || '')
                    .set("$top", params.$top || 10)
                    .set("$skip", params.$skip || 0)
                    .set("$inlinecount", params.$inlinecount || 'allpages')
            };
        }
        // UserSkillURL:    http://localhost:64738/api/users?$orderby=Name desc&$filter=substringof(' ', Name)&$top=10&skip=0&$inlinecount=allpages
        // Warehouse URL:    http://localhost:3000/api/documents?page=1&pageSize=10&sort=dateOfCreation&sortDirection=desc
        return this.http.get(usersUrl, queryString).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            return new _model_userSR__WEBPACK_IMPORTED_MODULE_4__["UserSR"](response);
        }));
    };
    UsersService.prototype.getAllSkills = function (params) {
        var queryString = {};
        if (params) {
            queryString = {
                params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                    .set("$orderby", params.$orderby || '')
                    .set("$filter", params.$filter || '')
                    .set("$top", params.$top || 10)
                    .set("$skip", params.$skip || 0)
                    .set("$inlinecount", params.$inlinecount || 'allpages')
            };
        }
        return this.http.get(skillsUrl, queryString).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            return new _model_skillSR__WEBPACK_IMPORTED_MODULE_5__["SkillSR"](response);
        }));
    };
    UsersService.prototype.addUser = function (user) {
        user.UserId = 0;
        return this.http.post(usersUrl, user).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (resp) {
            return new _model_user__WEBPACK_IMPORTED_MODULE_6__["User"](resp);
        }));
    };
    UsersService.prototype.addSkill = function (skill) {
        skill.SkillId = 0;
        return this.http.post(skillsUrl, skill).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (resp) {
            return new _model_skill__WEBPACK_IMPORTED_MODULE_7__["Skill"](resp);
        }));
    };
    UsersService.prototype.removeUser = function (id) {
        return this.http.delete(usersUrl + "/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return new _model_user__WEBPACK_IMPORTED_MODULE_6__["User"](response); }));
    };
    //DELETE api/users/{id}
    UsersService.prototype.removeSkill = function (id) {
        return this.http.delete(skillsUrl + "/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return new _model_skill__WEBPACK_IMPORTED_MODULE_7__["Skill"](response); }));
    };
    UsersService.prototype.getUser = function (id) {
        return this.http.get(usersUrl + "/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return new _model_user__WEBPACK_IMPORTED_MODULE_6__["User"](response); }));
    };
    UsersService.prototype.updateUser = function (editedUser) {
        return this.http.put(usersUrl + "/" + editedUser.UserId, editedUser).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return new _model_user__WEBPACK_IMPORTED_MODULE_6__["User"](response); }));
    };
    UsersService.prototype.getPhoto = function (user) {
        console.log(user.Photo);
        if (user.Photo) {
            return this.http.get("http://localhost:64738/" + user.Photo, { responseType: 'blob' });
        }
        else {
            console.log("Service ne dobija photoUrl.");
        }
    };
    //registracija se obavlja na serveru i to je post metod kojim se salju vrednosti register forme user, pass i confirmPass
    UsersService.prototype.registerUser = function (credentials) {
        return this.http.post('http://localhost:64738/api/Account/register', credentials);
    };
    UsersService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UsersService);
    return UsersService;
}());



/***/ }),

/***/ "./src/app/users/skills-table/skills-table.component.css":
/*!***************************************************************!*\
  !*** ./src/app/users/skills-table/skills-table.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL3NraWxscy10YWJsZS9za2lsbHMtdGFibGUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/users/skills-table/skills-table.component.html":
/*!****************************************************************!*\
  !*** ./src/app/users/skills-table/skills-table.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col\">\n      <us-filter [flagSkill]=\"flagSkill\" (onSearchBySkill)=\"searchBySkill($event)\"></us-filter>\n    </div>\n  </div>\n<div class=\"row\">\n  <div class=\"col-2\"></div>\n  <div class=\"col-8\">\n    <table class=\"table table-striped\">\n      <thead class=\"thead-default\">\n        <tr>\n          <th class=\"text-center\" colspan=\"4\">\n            SkillName\n            <button class=\"btn btn-link\" (click)=\"onSort('SkillName')\"><i class=\"fa fa-sort\"></i></button>\n          </th>\n        </tr>\n      </thead>\n      <tbody>\n\n        <tr *ngFor=\"let skill of skills?.Items\">\n          <td class=\"text-center\" colspan=\"4\">{{skill.SkillName}}</td>\n          <td><button class=\"btn btn-link\" (click)=\"onDelete(skill.SkillId)\"><i class=\"fa fa-remove\"></i></button></td>\n          <td><button class=\"btn btn-link\" [routerLink]=\"['/skills/', skill.SkillId]\"><i\n                class=\"fa fa-edit\"></i></button></td>\n        </tr>\n      </tbody>\n    </table>\n\n  </div>\n  <div class=\"col-2\"></div>\n</div>\n<div class=\"row\">\n  <div class=\"col\">\n    <us-paging></us-paging>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/users/skills-table/skills-table.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/users/skills-table/skills-table.component.ts ***!
  \**************************************************************/
/*! exports provided: SkillsTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SkillsTableComponent", function() { return SkillsTableComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/users.service */ "./src/app/users/service/users.service.ts");



var SkillsTableComponent = /** @class */ (function () {
    function SkillsTableComponent(skillService) {
        this.skillService = skillService;
        this.params = {
            $orderby: 'SkillName',
            $filter: "substringof('', SkillName)",
            $top: 10,
            $skip: 0,
            $inlinecount: 'allpages'
        };
        this.flagSkill = 1;
    }
    SkillsTableComponent.prototype.ngOnInit = function () {
        this.updateSkills();
    };
    SkillsTableComponent.prototype.updateSkills = function () {
        var _this = this;
        this.skillService.getAllSkills(this.params).subscribe(function (data) { return _this.skills = data; });
    };
    SkillsTableComponent.prototype.onDelete = function (id) {
        var _this = this;
        this.skillService.removeSkill(id).subscribe(function (skill) { _this.updateSkills(); });
    };
    SkillsTableComponent.prototype.onDetails = function () {
    };
    SkillsTableComponent.prototype.onSort = function (criteria) {
        if (this.params.$orderby == criteria) {
            if (this.params.$orderby.includes('desc')) {
                this.params.$orderby = criteria;
            }
            else {
                this.params.$orderby = criteria + " desc";
            }
        }
        else {
            this.params.$orderby = criteria;
        }
        this.updateSkills();
    };
    SkillsTableComponent.prototype.searchBySkill = function (searchString) {
        this.params.$filter = "substringof('" + searchString + "', SkillName)";
        this.updateSkills();
    };
    SkillsTableComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'us-skills-table',
            template: __webpack_require__(/*! ./skills-table.component.html */ "./src/app/users/skills-table/skills-table.component.html"),
            styles: [__webpack_require__(/*! ./skills-table.component.css */ "./src/app/users/skills-table/skills-table.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"]])
    ], SkillsTableComponent);
    return SkillsTableComponent;
}());



/***/ }),

/***/ "./src/app/users/users-table/users-table.component.css":
/*!*************************************************************!*\
  !*** ./src/app/users/users-table/users-table.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL3VzZXJzLXRhYmxlL3VzZXJzLXRhYmxlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/users/users-table/users-table.component.html":
/*!**************************************************************!*\
  !*** ./src/app/users/users-table/users-table.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col\">\n    <us-filter [flagEmail]=\"1\" (onSearchByEmail)=\"searchByEmail($event)\" ></us-filter>\n  </div>\n</div>\n<div class=\"row\">\n  <div class=\"col\">\n\n    <table class=\"table table-striped\">\n      <thead class=\"thead-default\">\n        <tr>\n          <th>\n            Name\n            <button class=\"btn btn-link\" (click)=\"onSort('Name')\"><span class=\"fa fa-sort\"></span></button>\n          </th>\n          <th>\n            Email\n            <button class=\"btn btn-link\" (click)=\"onSort('Email')\"><span class=\"fa fa-sort\"></span></button>\n          </th>\n          <th>\n            Password\n            <button class=\"btn btn-link\" (click)=\"onSort('Password')\"><span class=\"fa fa-sort\"></span></button>\n          </th>\n\n        </tr>\n      </thead>\n      <tbody>\n\n        <tr *ngFor=\"let user of users\" >\n          <td (click)=\"onDetails(user)\">{{user.Name}}</td>\n          <td (click)=\"onDetails(user)\">{{user.Email}}</td>\n          <td (click)=\"onDetails(user)\">{{user.Password}}</td>\n          <td><button class=\"btn btn-link\" (click)=\"onDelete(user.UserId)\"><span class=\"fa fa-remove\"></span></button></td>\n          <td><button class=\"btn btn-link\"   [routerLink]=\"['/users/', user.UserId]\"><span class=\"fa fa-edit\"></span></button></td>\n          \n        </tr>\n      </tbody>\n    </table>\n\n  </div>\n</div>\n<div class=\"row\">\n    <div class=\"col\">\n      <us-paging [collectionSize]=\"count\" (onPageSelected)=\"onPageSelected($event)\"></us-paging>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/users/users-table/users-table.component.ts":
/*!************************************************************!*\
  !*** ./src/app/users/users-table/users-table.component.ts ***!
  \************************************************************/
/*! exports provided: UsersTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersTableComponent", function() { return UsersTableComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/users.service */ "./src/app/users/service/users.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");






var UsersTableComponent = /** @class */ (function () {
    function UsersTableComponent(userService, router, ngbService, location) {
        this.userService = userService;
        this.router = router;
        this.ngbService = ngbService;
        this.location = location;
        this.users = []; //objekat tipa SearchResult: count + User[]  
        this.count = 0;
        //server sortira na osnovu $orderby ako je samo criterium onda je asscending ; 
        //server filtrira na osnovu $filter tako sto mu se menja searchString i polje modela;
        // 
        this.params = {
            $orderby: 'Name desc',
            $filter: "substringof('', Name)",
            $top: 5,
            $skip: 0,
            $inlinecount: 'allpages'
        };
        this.flagEmail = 0;
    }
    UsersTableComponent.prototype.ngOnInit = function () {
        this.refreshUsers();
        // @Input parametar collectionSize  = count 
        this.checkCount = this.count;
        this.ngbService.pageSize = this.params.$top; //pageSize u ngb se podesava preko ngbService
        console.log("Iz UsersTableComp ngOnInit   Count= ", this.count, "   maxSize=", this.ngbService.maxSize);
    };
    UsersTableComponent.prototype.refreshUsers = function () {
        var _this = this;
        this.userService.getAll(this.params).subscribe(function (usersSR) {
            _this.count = usersSR.Count;
            _this.ngbService.maxSize = Math.ceil(_this.count / _this.params.$top); //maxSize je u ngb NumberOfPages
            console.log("Iz UserTableComp ngOnInit-RefreshUsers   Count= ", _this.count, "   maxSize=", _this.ngbService.maxSize);
            _this.users = usersSR.Items;
            if (_this.count != _this.checkCount) {
                _this.checkCount = _this.count;
                _this.location.go('users');
            }
        });
    };
    UsersTableComponent.prototype.onDelete = function (id) {
        var _this = this;
        this.userService.removeUser(id).subscribe(function (data) { _this.refreshUsers(); });
    };
    UsersTableComponent.prototype.onDetails = function (user) {
        this.router.navigate(['users/details/', user.UserId]);
    };
    UsersTableComponent.prototype.onSort = function (criteria) {
        if (this.params.$orderby == criteria) {
            if (this.params.$orderby.includes('desc')) {
                this.params.$orderby = criteria;
            }
            else {
                this.params.$orderby = criteria + " desc";
            }
        }
        else {
            this.params.$orderby = criteria;
        }
        this.refreshUsers();
    };
    UsersTableComponent.prototype.searchByEmail = function (searchString) {
        this.params.$filter = "substringof('" + searchString + "', Email)";
        this.refreshUsers();
    };
    UsersTableComponent.prototype.onPageSelected = function (selectedPage) {
        this.params.$skip = (selectedPage - 1) * this.params.$top;
        this.refreshUsers();
    };
    UsersTableComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'us-users-table',
            template: __webpack_require__(/*! ./users-table.component.html */ "./src/app/users/users-table/users-table.component.html"),
            styles: [__webpack_require__(/*! ./users-table.component.css */ "./src/app/users/users-table/users-table.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbPaginationConfig"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"]])
    ], UsersTableComponent);
    return UsersTableComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\DejanJovancevic\Angular\UserSkill\USERSKILL2\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map