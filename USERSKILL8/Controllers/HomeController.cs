﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using static Common.CustomLogging;

namespace USERSKILL8.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public HttpResponseMessage Test()
        {
            string _mess = "Web service is working";

            CustomLogging.LogMessage(TracingLevel.INFO, _mess);

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new StringContent(_mess, System.Text.Encoding.UTF8, "text/plain");
            return resp;
        }
    }
}
