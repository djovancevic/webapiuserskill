﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using USERSKILL8.Interfaces;
using USERSKILL8.Models;

namespace USERSKILL8.Controllers
{
    public class SkillsController : ApiController
    {
        ISkillRepo _repository { get; set; }

        public SkillsController(ISkillRepo repository)
        {
            this._repository = repository;
        }

        //GET api/skills
        //[EnableQueryAttribute(PageSize = 50)]
        //public IEnumerable<Skill> Default()
        //{
        //    return _repository.GetAll();
        //}

        //GET api/users
        public PageResult<Skill> Get(ODataQueryOptions<Skill> queryOptions)
        {
            IQueryable results = queryOptions.ApplyTo(_repository.GetAll());
            return new PageResult<Skill>(results as IEnumerable<Skill>, Request.GetNextPageLink(), Request.GetInlineCount());
        }

        //GET api/skills/{id}
        [ResponseType(typeof(Skill))]
        public IHttpActionResult Get(int id)
        {
            var skill = _repository.GetById(id);
            if (skill == null)
            {
                return NotFound();
            }
            return Ok(skill);
        }

        //POST api/skills
        //[Authorize]
        [ResponseType(typeof(Skill))]
        public IHttpActionResult Post(Skill skill)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(skill);
            return CreatedAtRoute("DefaultApi", new { id = skill.SkillId }, skill);
        }
        //PUT api/skills/{id}
        [ResponseType(typeof(Skill))]
        public IHttpActionResult Put(int id, Skill skill)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != skill.SkillId)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(skill);
            }
            catch (Exception)
            {

                return BadRequest();
            }

            return Ok(skill);
        }

        //DELETE api/skills/{id}
        [ResponseType(typeof(void))]
        public IHttpActionResult Delete(int id)
        {
            var skill = _repository.GetById(id);
            if (skill == null)
            {
                return NotFound();
            }

            _repository.Delete(skill);
            return Ok();
        }

        //GET api/filter-by-first-letter
        [Authorize]
        [Route("api/filter-skills-by-first-letter")]
        [HttpGet]
        public IQueryable<Skill> FilterByFirstLetter(String startsWith)
        {
            var list = _repository.ByFirstLetter(startsWith);
            return list;
        }


        //GET api/search-skills-in-a-letter-range
        [Authorize]
        [Route("api/search-skills-in-a-letter-range")]
        [HttpPost]
        public IQueryable<Skill> SearchInLetterRange(Char firstChar, Char lastChar)
        {
            var list = _repository.ByFirstLetterRange(firstChar, lastChar);
            return list;
        }
    }
}
