﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using USERSKILL8.Interfaces;
using USERSKILL8.Models;
using System.Web;
using System.IO;
using Common;
using static Common.CustomLogging;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Net;
using System.Web.Configuration;
using Newtonsoft.Json;

namespace USERSKILL8.Controllers
{
    public class UsersController : ApiController
    {
        IUserRepo _repository { get; set; }

        public UsersController(IUserRepo repository)
        {
            this._repository = repository;
        }



        //GET api/users
        public PageResult<User> Get(ODataQueryOptions<User> queryOptions)
        {
            IQueryable results = queryOptions.ApplyTo(_repository.GetAll());

            // log OK
            //log OK
            string _mess = $"Returning HTTP 200 OK.";
            CustomLogging.LogMessage(TracingLevel.INFO, _mess);


            return new PageResult<User>(results as IEnumerable<User>, Request.GetNextPageLink(), Request.GetInlineCount());
        }




        //GET api/users/{id}
        [ResponseType(typeof(User))]
        public IHttpActionResult Get(int id)
        {
            var user = _repository.GetById(id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        //POST api/users
        [Authorize]
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> Post()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            User user = new User();
            var provider = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());
            //access form data : these are non-file data collection
            NameValueCollection formData = provider.FormData;
            //access files  : these are file data collection
            IList<HttpContent> files = provider.Files;

            HttpContent file1 = files[0];
            var thisFileName = file1.Headers.ContentDisposition.FileName;
            if (thisFileName.StartsWith("\"") && thisFileName.EndsWith("\""))
            {
                thisFileName = thisFileName.Trim('"');
            }
            if (thisFileName.Contains(@"/") || thisFileName.Contains(@"\"))
            {
                thisFileName = Path.GetFileName(thisFileName);
            }



            string filename = String.Empty;
            string directoryName = String.Empty;
            string URL = String.Empty;
            Stream input = await file1.ReadAsStreamAsync();
            string tempDocUrl = WebConfigurationManager.AppSettings["DocsUrl"];
            string userPhoto = String.Empty;

            // inicijalizujem objekat user
            string name = formData.Get("Name");
            string email = formData.Get("Email");
            string password = formData.Get("Password");
            bool picture = bool.Parse(formData.Get("Picture"));
            List<Skill> skills = JsonConvert.DeserializeObject<List<Skill>>(formData.Get("Skills"));
            // "Skills": [ { "SkillId": 1, "SkillName": "C# programer", "Users": [] }, { "SkillId": 16, "SkillName": "Data analitics", "Users": [] }, { "SkillId": 4, "SkillName": "Java programer", "Users": [] }, { "SkillId": 12, "SkillName": "MixMarketer", "Users": [] } ] }
            user = new User { Name = name, Email = email, Password = password, Picture = picture, Skills = skills };

            if (picture == true)
            {
                var path = HttpRuntime.AppDomainAppPath;
                directoryName = System.IO.Path.Combine(path, "Content\\Photos");

                //izmena thisFileName u shemu mirkomirkovic.jpeg
                    if (Path.HasExtension(thisFileName))
                {
                    string fileExtension = Path.GetExtension(thisFileName);
                    string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(thisFileName);
                    fileNameWithoutExtension = user.Name;
                    fileNameWithoutExtension.Replace(" ", string.Empty);
                    thisFileName = fileNameWithoutExtension + fileExtension;   // mirkomirkovic.jpg
                }

                // file premestamo na novu file path i brisemo stari file
                filename = System.IO.Path.Combine(directoryName, thisFileName);

                //Deletion exists file
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }

                string DocsPath = tempDocUrl + "/" + "Content//Photos" + "/";
                URL = DocsPath + thisFileName;
                userPhoto = "Content/Photos/" + thisFileName;

            }


            //Directory.CreateDirectory(@directoryName);
            using (Stream file = File.OpenWrite(filename))
            {
                input.CopyTo(file);
                //close file
                file.Close();
            }

            user.Photo = userPhoto;

            // rad sa EF repository
            if (!ModelState.IsValid)
            {
                // log exception

                string _message = $"Returning HTTP 400 Bad Request for 1. UserId = {ModelState.IsValidField("UserId")}, 2. Skills = {ModelState.IsValidField("Skills")}, 3. Photo = {ModelState.IsValidField("Photo")}";
                CustomLogging.LogMessage(TracingLevel.INFO, _message);

                return BadRequest(ModelState);
            }


            _repository.Add(user);

            //log OK
            string _mess = $"Returning HTTP 200 OK. 1. UserId = {ModelState.IsValidField("UserId")}, 2. Skills = {ModelState.IsValidField("Skills")}, 3. Photo = {ModelState.IsValidField("Photo")}";
            CustomLogging.LogMessage(TracingLevel.INFO, _mess);

            //return CreatedAtRoute("DefaultApi", new { id = user.UserId }, user);
            return Ok();


        }




        //PUT api/users/{id}


        [Authorize]
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> Put()
        {

            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            User user = new User();
            var provider = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());
            //access form data : these are non-file data collection
            NameValueCollection formData = provider.FormData;
            //access files  : these are file data collection
            IList<HttpContent> files = provider.Files;

            HttpContent file1 = files[0];
            var thisFileName = file1.Headers.ContentDisposition.FileName;
            if (thisFileName.StartsWith("\"") && thisFileName.EndsWith("\""))
            {
                thisFileName = thisFileName.Trim('"');
            }
            if (thisFileName.Contains(@"/") || thisFileName.Contains(@"\"))
            {
                thisFileName = Path.GetFileName(thisFileName);
            }



            string filename = String.Empty;
            string directoryName = String.Empty;
            string URL = String.Empty;
            Stream input = await file1.ReadAsStreamAsync();
            string tempDocUrl = WebConfigurationManager.AppSettings["DocsUrl"];
            string userPhoto = String.Empty;

            // inicijalizujem objekat user
            bool isInt = int.TryParse(formData.Get("UserId"), out int id);
            string name = formData.Get("Name");
            string email = formData.Get("Email");
            string password = formData.Get("Password");
            bool picture = bool.Parse(formData.Get("Picture"));
            List<Skill> skills = JsonConvert.DeserializeObject<List<Skill>>(formData.Get("Skills"));
            // "Skills": [ { "SkillId": 1, "SkillName": "C# programer", "Users": [] }, { "SkillId": 16, "SkillName": "Data analitics", "Users": [] }, { "SkillId": 4, "SkillName": "Java programer", "Users": [] }, { "SkillId": 12, "SkillName": "MixMarketer", "Users": [] } ] }
            user = new User { UserId = id, Name = name, Email = email, Password = password, Picture = picture, Skills = skills };

            if (picture == true)
            {
                var path = HttpRuntime.AppDomainAppPath;
                directoryName = System.IO.Path.Combine(path, "Content\\Photos");

                //izmena thisFileName u shemu mirkomirkovic.jpeg
                if (Path.HasExtension(thisFileName))
                {
                    string fileExtension = Path.GetExtension(thisFileName);
                    string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(thisFileName);
                    fileNameWithoutExtension = user.Name;
                    fileNameWithoutExtension.Replace(" ", string.Empty);
                    thisFileName = fileNameWithoutExtension + fileExtension;   // mirkomirkovic.jpg
                }

                // file premestamo na novu file path i brisemo stari file
                filename = System.IO.Path.Combine(directoryName, thisFileName);

                //Deletion exists file
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }

                string DocsPath = tempDocUrl + "/" + "Content//Photos" + "/";
                URL = DocsPath + thisFileName;
                userPhoto = "Content/Photos/" + thisFileName;

            }


            //Directory.CreateDirectory(@directoryName);
            using (Stream file = File.OpenWrite(filename))
            {
                input.CopyTo(file);
                //close file
                file.Close();
            }

            user.Photo = userPhoto;



            //rad sa EF repository

            if (!ModelState.IsValid)
            {
                // log exception

                string _mess = $"Returning HTTP 400 Bad Request for 1. UserId = {ModelState.IsValidField("UserId")}, 2. Skills = {ModelState.IsValidField("Skills")}, 3. Photo = {ModelState.IsValidField("Photo")}";
                CustomLogging.LogMessage(TracingLevel.INFO, _mess);

                return BadRequest(ModelState);
            }

            if (id != user.UserId)
            {
                // log exception

                string _mess = $"Returning HTTP 400 Bad Request for 1. UserId = {user.UserId}";
                CustomLogging.LogMessage(TracingLevel.INFO, _mess);

                return BadRequest();
            }


            try
            {
                _repository.Update(user);
            }
            catch (Exception)
            {

                return BadRequest();
            }

            return Ok(user);
        }

        //DELETE api/users/{id}
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult Delete(int id)
        {
            var user = _repository.GetById(id);
            if (user == null)
            {
                return NotFound();
            }

            _repository.Delete(user);
            return Ok();
        }

        

    }
}
