﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USERSKILL8.Models;

namespace USERSKILL8.Interfaces
{
    public interface ISkillRepo
    {
        IQueryable<Skill> GetAll();
        Skill GetById(int Id);
        void Add(Skill skill);
        void Update(Skill skill);
        void Delete(Skill skill);
        IQueryable<Skill> ByFirstLetter(String starstWith);
        IQueryable<Skill> ByFirstLetterRange(Char firstChar, Char lastChar);
    }
}
