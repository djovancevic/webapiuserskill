﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USERSKILL8.Models;

namespace USERSKILL8.Interfaces
{
    public interface IUserRepo
    {
        IQueryable<User> GetAll();
        User GetById(int Id);
        void Add(User user);
        void Update(User user);
        void Delete(User user);
        //IQueryable<User> ByFirstLetter(string startsWith);
        //IQueryable<User> ByFirstLetterRange(Char firstChar, Char lastChar);
    }
}
