namespace USERSKILL8.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using USERSKILL8.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<USERSKILL8.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(USERSKILL8.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            var user1 = new User() {  Name = "Marko Markovic", Email = "marko@gmail.com", Password = "Sifra.123", Photo = "content/Photos/marko.jpg", Skills = new List<Skill>() };
            var user2 = new User() {  Name = "Janko Jankovic", Email = "janko@gmail.com", Password = "Sifra.123", Photo = "content/Photos/janko.jpg", Skills = new List<Skill>() };
            var user3 = new User() {  Name = "Mirko Mirkovic", Email = "mirko@gmail.com", Password = "Sifra.123", Photo = "content/Photos/mirko.jpg", Skills = new List<Skill>() };
            var user4 = new User() {  Name = "Zdravko Zdravkovic", Email = "zdravko@gmail.com", Password = "Sifra.123", Photo = "content/Photos/zdravo.jpg", Skills = new List<Skill>() };
            var user5 = new User() {  Name = "Veljko Veljkovic", Email = "veljko@gmail.com", Password = "Sifra.123", Photo = "content/Photos/veljko.jpg", Skills = new List<Skill>() };


            // u promenljivama skill su objekti klase Skill
            var skill1 = new Skill() { SkillName = "C# programer", Users = new List<User>() };
            var skill2 = new Skill() { SkillName = "Java programer", Users = new List<User>() };
            var skill3 = new Skill() { SkillName = "TypeScript programer", Users = new List<User>() };
            var skill4 = new Skill() { SkillName = "PHP programer", Users = new List<User>() };
            var skill5 = new Skill() { SkillName = "Pyton programer", Users = new List<User>() };
            var skill6 = new Skill() { SkillName = "Web Designer", Users = new List<User>() };


            //sada u listu skilova svakog usera dodajemo odgovarajuce skillove, jos ne pozivamo klasu context
            user1.Skills.Add(skill1);
            user2.Skills.Add(skill2);
            user3.Skills.Add(skill3);
            //SA 3 LEJE RADI SA 4 NE RADI, IAKO JE LOGIKA ISTA ???
            user4.Skills.Add(skill4);
            user5.Skills.Add(skill5);
            user1.Skills.Add(skill3);
            user1.Skills.Add(skill6);
            user2.Skills.Add(skill4);
            user2.Skills.Add(skill5);


            //sada u listu usera svakog skilla dodajemo naspramne usere, jos ne pozivamo klasu context
            skill1.Users.Add(user1);
            skill2.Users.Add(user2);
            skill3.Users.Add(user3);
            skill3.Users.Add(user1);
            skill4.Users.Add(user4);
            skill4.Users.Add(user2);
            skill5.Users.Add(user5);
            skill5.Users.Add(user2);
            skill6.Users.Add(user1);

            //sada tek pozivamo EF klasu context i setove Users i Skills jer imamo potpuno formirane sve usere i skillove koje iniciramo kao posebne promenljive koje stimaju
            context.Users.AddOrUpdate(user1);
            context.Users.AddOrUpdate(user2);
            context.Users.AddOrUpdate(user3);
            context.Users.AddOrUpdate(user4);
            context.Users.AddOrUpdate(user5);

            context.Skills.AddOrUpdate(skill1);
            context.Skills.AddOrUpdate(skill2);
            context.Skills.AddOrUpdate(skill3);
            context.Skills.AddOrUpdate(skill4);
            context.Skills.AddOrUpdate(skill5);
            context.Skills.AddOrUpdate(skill6);

            context.SaveChanges();
        }
    }
}
