﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace USERSKILL8.Models
{
    public class Skill
    {
        public Skill()
        {
            this.Users = new List<User>();
        }

        public int SkillId { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string SkillName { get; set; }

        //public int UserRefId { get; set; }
        //[ForeignKey("UserRefId")]
        public virtual ICollection<User> Users { get; set; }

        

        public bool CompareToSkill(Char firstChar, Char lastChar)
        {
            bool belongsToRange = false;
            int precedes = SkillName.First().ToString().CompareTo(firstChar);
            int follow = SkillName.First().ToString().CompareTo(lastChar);
            if (!(precedes < 0 && follow > 0))
            {
                belongsToRange = true;
            }
            return belongsToRange;
        }
    }
}