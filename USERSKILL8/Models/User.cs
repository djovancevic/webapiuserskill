﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace USERSKILL8.Models
{
    public class User
    {
        public User()
        {
            this.Skills = new List<Skill>();
            this.Picture = true;
            this.Photo = "";
        }

        public int UserId { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 6)]//mogao bi biti RegularExpression da sadrzi @
        public string Email { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 8)]// mogao bi bit zahtev koje vrste karaktera treba da sadrzi password
        public string Password { get; set; }
        public bool Picture { get; set; }
        [Required]
        public string Photo { get; set; }

        //public int SkillRefId { get; set; }
        //[ForeignKey("SkillRefId")]
        public virtual ICollection<Skill> Skills { get; set; }

          
    }
}