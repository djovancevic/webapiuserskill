﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using USERSKILL8.Interfaces;
using USERSKILL8.Models;

namespace USERSKILL8.Repository
{
    public class SkillsRepo : IDisposable, ISkillRepo
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        public IQueryable<Skill> GetAll()
        {
            return db.Skills.Include(s => s.Users).ToList<Skill>().AsQueryable<Skill>();         //SqlQuery(" SELECT * FROM Skills, Users, UserSkills WHERE Skills.SkillId = UserSkills.Skill_SkillId AND Users.UserId = UserSkills.User_UserId").ToList<Skill>().AsQueryable(); 
                                                                           // Include(s => s.Users).OrderByDescending(s => s.SkillName).ToList<Skill>().AsQueryable<Skill>();
        }

        public Skill GetById(int id)
        {
            return db.Skills.Include(s => s.Users).Where(s => s.SkillId == id).FirstOrDefault<Skill>();
        }

        public void Add(Skill skill)
        {
            db.Skills.Add(skill);
            //resavanja problema dupliranja Usera u database
            foreach (var user in skill.Users)
                db.Entry(skill).State = user.UserId == 0 ? EntityState.Added : EntityState.Modified;
            db.SaveChanges();
        }

        public void Update(Skill skill)
        {
            db.Entry(skill).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }
        }

        public void Delete(Skill skill)
        {
            db.Skills.Remove(skill);
            db.SaveChanges();

        }

        public IQueryable<Skill> ByFirstLetter(String startsWith)
        {
            var filterByFirstLetter = db.Skills.Where(s => s.SkillName.ToUpper().StartsWith(startsWith.ToUpper())).OrderBy(s => s.SkillName);
            return filterByFirstLetter;
        }

        public IQueryable<Skill> ByFirstLetterRange(Char firstChar, Char lastChar)
        {
            var searchByFirstLetterRange = db.Skills.Where(s => s.CompareToSkill(firstChar, lastChar));
            return searchByFirstLetterRange;
        }


        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}