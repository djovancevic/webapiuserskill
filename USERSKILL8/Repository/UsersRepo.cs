﻿using RefactorThis.GraphDiff;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using USERSKILL8.Interfaces;
using USERSKILL8.Models;

namespace USERSKILL8.Repository
{
    public class UsersRepo : IDisposable, IUserRepo
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public IQueryable<User> GetAll()
        {
            return db.Users.Include(u => u.Skills).OrderBy(u => u.UserId).ToList<User>().AsQueryable<User>(); //Include(u => u.Skills).OrderByDescending(u => u.Name).ToList<User>().AsQueryable<User>();
        }

        public User GetById(int id)
        {
            return db.Users.Include(u => u.Skills).Where(u => u.UserId == id).FirstOrDefault<User>();
        }

        public void Add(User user)
        {
            db.Users.Add(user);
            //nakon sto je entity user postavljen u state = added
            //u foreach petlji za sve skilove ciji SkillId nije nula, prebacujemo stanje sa added(sto je defaultno kao navProp od User) na Modified, tako da nece dodavati nove skillove u tabelu skillova i u veznu tabelu... 
            foreach (var skill in user.Skills)
                db.Entry(skill).State = skill.SkillId == 0 ? EntityState.Added : EntityState.Modified;
            db.SaveChanges();
        }

        public void Update(User editedUser)
        {

            db.UpdateGraph(editedUser, user => user
                .AssociatedCollection(p => p.Skills));


            db.SaveChanges();
        }

        public void Delete(User user)
        {
            db.Users.Remove(user);
            db.SaveChanges();

        }

        


        //dispose metoda

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}